package com.doandcompany.taipei25.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.doandcompany.taipei25.dao.NaverJoongonaraArticle;
import com.doandcompany.taipei25.dao.NaverJoongonaraCategory;

@Repository(value="Taipei25Mapper")
public interface Taipei25Mapper {
	
	void addNaverJoongonaraArticle(NaverJoongonaraArticle naverJoongonaraArticle);
	
	void addNaverJoongonaraCategory(NaverJoongonaraCategory naverJoongonaraCategory);
	
	List<NaverJoongonaraCategory> getNaverJoongonaraCategory();
	
	List<NaverJoongonaraArticle> getNaverJoongonaraArticle(int categoryIdx);

	List<NaverJoongonaraArticle> getNaverJoongonaraArticleAll();
	
	
}
