package com.doandcompany.taipei25.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.doandcompany.taipei25.dao.NaverJoongonaraCategory;
import com.doandcompany.taipei25.mapper.Taipei25Mapper;
import com.doandcompany.taipei25.service.NaverJoongonaraManager;
import com.mongodb.BasicDBObject;

@RestController
@RequestMapping("/")
public class Taipei25Controller {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private NaverJoongonaraManager naverJoongonaraManager;
	
	@Autowired
	Taipei25Mapper taipei25Mapper;
	
	@RequestMapping(value = "/crawlJoongonara", method = RequestMethod.GET)
	public BasicDBObject crawlJoongonara(			
			@RequestParam(value = "pw", required = false) String pw) {

		logger.info("[crawlJoongonara]");
		
		if(pw== null || pw.equals("dudu1234")==false) return new BasicDBObject().append("status", "ok");
		
		Thread t = new Thread(new Runnable() {
			public void run() {

				logger.info("[CronjobService] Thread runs");
				// TODO Auto-generated method stub
				
				List<NaverJoongonaraCategory> categories=taipei25Mapper.getNaverJoongonaraCategory();
				
				for(NaverJoongonaraCategory category:categories) {
					for(int i=6;i<65;i++)						
						naverJoongonaraManager.process(category.getIdx(), i);		
					
						//1분동안 쉰다.
						try {
							Thread.sleep(60000);
						} catch (InterruptedException e) {
						} 
					}
				}
		});
		
		t.start();
		
		return new BasicDBObject().append("status", "working");
		//return openChatManager.signupUser(deviceId, userId,pushToken, pushType);
	}

	@RequestMapping(value = "/crawlJoongonaraCategory", method = RequestMethod.GET)
	public BasicDBObject crawlJoongonaraCategory(			
			@RequestParam(value = "pw", required = false) String pw) {

		logger.info("[crawlJoongonaraCategory]");
		
		if(pw== null || pw.equals("dudu1234")==false) return new BasicDBObject().append("status", "ok");
		
		Thread t = new Thread(new Runnable() {
			public void run() {

				logger.info("[CronjobService] Thread runs");
				// TODO Auto-generated method stub
				
				for(int i=4;i<15;i++)
					naverJoongonaraManager.processMenuList(i);
			}
		});
		
		t.start();
		
		return new BasicDBObject().append("status", "working");
		//return openChatManager.signupUser(deviceId, userId,pushToken, pushType);
	}

	@RequestMapping(value = "/analyzeJoongonara", method = RequestMethod.GET)
	public Map<String, Integer> analyzeJoongonara(@RequestParam(value = "category", required = true) int category) {

		logger.info("[analyzeJoongonara]");
		
		return naverJoongonaraManager.analyze(category);		
	}
	
}