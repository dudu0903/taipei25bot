package com.doandcompany.taipei25.dao;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Data;

@Data
public class NaverJoongonaraArticle {
	int idx;
	String title;
	String writer;
	String date;
	int hit;
	int isSold;
	int replyNum;
	int category;
	
	static Date now = new Date();
	static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	
	public void setDate(String date) {
		if(date.contains(":") == true) date =format.format(now);
		this.date = date;
	}
}
