package com.doandcompany.taipei25.dao;

import lombok.Data;

@Data
public class NaverJoongonaraCategory {
	int idx;
	String name;
}
