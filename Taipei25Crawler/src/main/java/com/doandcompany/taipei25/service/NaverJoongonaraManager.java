package com.doandcompany.taipei25.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.doandcompany.taipei25.dao.NaverJoongonaraArticle;
import com.doandcompany.taipei25.dao.NaverJoongonaraCategory;
import com.doandcompany.taipei25.mapper.Taipei25Mapper;

/**
 * A example that demonstrates how HttpClient APIs can be used to perform
 * form-based logon.
 */
@Service("NaverJoongonaraManager")
public class NaverJoongonaraManager {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	Taipei25Mapper taipei25Mapper;

	BasicCookieStore cookieStore = new BasicCookieStore();
	CloseableHttpClient httpclient = HttpClients.custom().setDefaultCookieStore(cookieStore).build();

	public static void main(String[] args) throws IOException {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring/application-config.xml");
		
		NaverJoongonaraManager naverJoongonaraManager = ctx.getBean(NaverJoongonaraManager.class);
		
		//naverJoongonaraManager.process(4);
		/*
		for(int i=0;i<1;i++)
			naverJoongonaraManager.processMenuList(i);
		*/
		BufferedWriter out = new BufferedWriter(new FileWriter("category.txt"));

		Map<String, Integer> map =naverJoongonaraManager.analyze(-1);
		
		for(Entry<String, Integer> entry:map.entrySet()) {
			String key =entry.getKey();
			Integer cnt = entry.getValue();
			
			if(key!=null && cnt!=null) {
				if(cnt.intValue()>10) {
				//System.out.println(key + " : "+cnt.toString() );
					out.write(key + " : "+cnt.toString());
					out.newLine();
				}
			}
		}
		
		out.close();
		
		System.exit(0);
	}
	
	public void processMenuList(int pagenum) {
		String url="http://m.cafe.naver.com/MenuListAjax.nhn?search.clubid=10050146&search.pagingType=more&search.page="+pagenum;
		Document doc;
		try {
			doc = Jsoup.connect(url).get();
			
			Elements rows = doc.select("a.tit");
			for (Element row : rows) {
				String uri = row.attr("href");
				String[] menuIds=uri.split("menuid=");
				if(menuIds==null || menuIds.length<2) continue;
			
				String menuIdTemp = menuIds[1];
				String[] menuIds2=menuIdTemp.split("&");				
				String menuId = menuIds2[0];								
				
				String name = row.text().replace(" New", "");
				
				NaverJoongonaraCategory naverJoongonaraCategory = new NaverJoongonaraCategory();
				naverJoongonaraCategory.setIdx(Integer.parseInt(menuId) );
				naverJoongonaraCategory.setName(name);
				taipei25Mapper.addNaverJoongonaraCategory(naverJoongonaraCategory);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(url);
		}
		
		return ;
		
	}
	
	public void process(int categoryIdx, int pagenum) {
		String url="http://cafe.naver.com/ArticleList.nhn?search.boardtype=L&userDisplay=50&search.menuid="+categoryIdx+"&search.questionTab=A&search.clubid=10050146&search.specialmenutype=&search.totalCount=2001&search.page="+pagenum+"&noticeHidden=true";
		
		Document doc;
		try {
			doc = Jsoup.connect(url).get();
			
			Elements rows = doc.select("table.board-box tbody tr");
			String[] items = new String[] { "번호", "제목", "", "", "작성자", "작성일", "조회수" };
			for (Element row : rows) {
				Iterator<Element> iterElem = row.getElementsByTag("td").iterator();
				String output = new String();
				NaverJoongonaraArticle naverJoongonaraArticle = new NaverJoongonaraArticle();
				naverJoongonaraArticle.setCategory(categoryIdx);
				int i = 0;

				while (iterElem.hasNext()) {
					Element element = iterElem.next();
					
					String content = element.text();
							
					
					if(i==0 && content.equals("")) break;
					
					
					switch(i) {
						case 0:
							try {
								int idx=Integer.parseInt(content);
								naverJoongonaraArticle.setIdx(Integer.parseInt(content));
							} catch (Exception e) {
								break;
							}
							
						break;
						
						case 1:
							Elements soldIcon=element.select("input.list-i-sellout");
							Elements replyNum=element.select("span.m-tcol-p");
							Elements title=element.select("a.m-tcol-c");
							
							naverJoongonaraArticle.setTitle(title.text());
							if(soldIcon.size()>0)
								naverJoongonaraArticle.setIsSold(1);
							
							if(replyNum.size()>0)
								naverJoongonaraArticle.setReplyNum(Integer.parseInt(replyNum.text().replace("[", "").replace("]", "")) ); 
						break;
						
						case 4:
							naverJoongonaraArticle.setWriter(content);
						break;
						
						case 5:
							naverJoongonaraArticle.setDate(content);
						break;
						
						case 6:
							naverJoongonaraArticle.setHit(Integer.parseInt(content));
						break;
						
					}
					
					i++;
					
					if(items.length<=i) break;
				}
				if(naverJoongonaraArticle.getIdx() >0 && i==items.length && naverJoongonaraArticle.getWriter()!=null ) {
					System.out.println(naverJoongonaraArticle.toString());
					System.out.println("--------------");
					
					try {
						taipei25Mapper.addNaverJoongonaraArticle(naverJoongonaraArticle);
					} catch(Exception e) {
						
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(url);
		}
		
		return ;
	}

	public Map<String, Integer> analyze(int categoryIdx) {

		HashMap<String, Integer> map = new HashMap<String, Integer>();
        
		ValueComparator bvc = new ValueComparator(map);
		TreeMap<String, Integer> sorted_map = new TreeMap<String, Integer>(bvc);
                
		List<NaverJoongonaraArticle> joongonaraArticleList =  null;
		
		if(categoryIdx==-1)
			joongonaraArticleList = taipei25Mapper.getNaverJoongonaraArticleAll();
		else 
			joongonaraArticleList = taipei25Mapper.getNaverJoongonaraArticle(categoryIdx);
		
		if(joongonaraArticleList != null) {
			for(NaverJoongonaraArticle article:joongonaraArticleList) {
				parseText(map, article.getTitle() );
			}
		}
		
		sorted_map.putAll(map);
		
		return sorted_map;
	}
		
	private void parseText(Map<String, Integer> map, String inputText) {
		String[] texts = inputText.split(" ");
		
		for(String text:texts) {
			addOne(map, text);
		}
	}
	
	private void addOne(Map<String, Integer> map, String text) {
		if(map.containsKey(text) == false) {
			map.put(text, 1);
		} else {
			map.put(text, map.get(text)+1);
		}
	}
}