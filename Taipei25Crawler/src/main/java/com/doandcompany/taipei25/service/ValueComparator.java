package com.doandcompany.taipei25.service;

import java.util.Comparator;
import java.util.Map;

public class ValueComparator implements Comparator {
    Map base;

    public ValueComparator(Map base) {
        this.base = base;
    }


	public int compare(Object o1, Object o2) {
		if ((Integer)base.get(o1) >= (Integer)base.get(o2)) {
            return -1;
        } else {
            return 1;
        } // returning 0 would merge keys
	}
}